package com.ftp.springFTP;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.net.ftp.FTPFile;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringFtpApplication {

	public static void main(String[] args) throws IOException {
		//SpringApplication.run(SpringFtpApplication.class, args);
		FtpClient client = new FtpClient();
		client.setServer("127.0.0.1");
		client.setPort(21);
		client.setPassword("root123");
		client.setUser("equus");
		client.getFtp();
		client.listFiles("/home/equus/").forEach(e->{
			System.out.println("list files with path"+e.getName());
		});
		System.out.println("/**************************************************/");
		client.listFiles("/home/equus/", "IMA").forEach(e->{
			System.out.println("list files ima"+e.getName());
		});
		System.out.println("/**************************************************/");
		client.getAllDirectory("/home/equus/").forEach(e->{
			System.out.println("Get all directory "+e.getName());
		});;
		System.out.println("/**************************************************/");
		client.getAllFile("/home/equus/").forEach(e->{
			System.out.println("Get all files "+e.getName());
		});
		System.out.println("/**************************************************/");
		System.out.println("uploadfiles "+client.uploadFile("/home/equus/","ima.csv"));
		System.out.println("/**************************************************/");
		System.out.println("downfile"+client.downloadFile("/home/equus/ima.csv", "ima1.csv"));
		System.out.println("/**************************************************/");
		System.out.println("is directory exist Android "+client.isDirectoryExist("/home/equus/", "Android"));
		System.out.println("/**************************************************/");
		System.out.println("is file exist ima.csv "+client.isFileExist("/home/equus/", "ima.csv"));
		
	}

}
