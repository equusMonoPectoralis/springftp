package com.ftp.springFTP;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.springframework.util.StringUtils;

public class FtpClient {

	private String server;
	private int port;
	private String user;
	private String password;
	private static FTPClient ftp = null;

	/**
	 * <p>
	 * Close {@link org.apache.commons.net.ftp.FTPClient} if it is not null.
	 * 
	 * @throws IOException or close {@link org.apache.commons.net.ftp.FTPClient}.
	 */
	public void close() throws IOException {
		if (ftp != null) {
			ftp.disconnect();
		}
	}

	/**
	 * <p>
	 * List files and directory with the specific path.
	 * 
	 * @param path to list.
	 * @return collection of {@link org.apache.commons.net.ftp.FTPFile}
	 * @throws IOException or return the collection.
	 */
	public Collection<FTPFile> listFiles(String path) throws IOException {
		FTPFile[] files = ftp.listFiles(path);
		return Arrays.stream(files).collect(Collectors.toList());
	}

	/**
	 * <p>
	 * List files and directory with the specific path and prefix.
	 * 
	 * @param path to list.
	 * @return collection of {@link org.apache.commons.net.ftp.FTPFile}
	 * @throws IOException or return the collection.
	 */
	public Collection<FTPFile> listFiles(String path, String prefix) throws IOException {
		return listFiles(path).stream().filter(e -> StringUtils.startsWithIgnoreCase(e.getName(), prefix))
				.collect(Collectors.toList());
	}

	/**
	 * <p>
	 * List all files with the specific path and prefix.
	 * 
	 * @param path to list.
	 * @return collection of {@link org.apache.commons.net.ftp.FTPFile}
	 * @throws IOException or return the collection.
	 */
	public Collection<FTPFile> getAllFile(String path) throws IOException {
		return listFiles(path).stream().filter(e -> e.isFile()).collect(Collectors.toList());
	}

	/**
	 * <p>
	 * List all directory with the specific path .
	 * 
	 * @param path to list.
	 * @return collection of {@link org.apache.commons.net.ftp.FTPFile}
	 * @throws IOException or return the collection.
	 */
	public Collection<FTPFile> getAllDirectory(String path) throws IOException {
		return listFiles(path).stream().filter(e -> e.isDirectory()).collect(Collectors.toList());
	}

	/**
	 * <p>
	 * List all directory with the specific path and prefix.
	 * 
	 * @param path to list.
	 * @return collection of {@link org.apache.commons.net.ftp.FTPFile}
	 * @throws IOException or return the collection.
	 */
	public boolean isDirectoryExist(String path, String directory) throws IOException {
		return !getAllDirectory(path).stream().filter(e -> e.getName().toLowerCase().equals(directory.toLowerCase()))
				.collect(Collectors.toList()).isEmpty();
	}

	/**
	 * 
	 * @param path
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public boolean isFileExist(String path, String file) throws IOException {
		return !getAllFile(path).stream().filter(e -> e.getName().toLowerCase().equals(file.toLowerCase()))
				.collect(Collectors.toList()).isEmpty();
	}

	public boolean uploadFile(String path,String name) throws FileNotFoundException, IOException {
		File file = new File(name);
		return ftp.storeFile(path, new FileInputStream(file));
	}

	public boolean downloadFile(String source, String destination) throws IOException {
		FileOutputStream out = new FileOutputStream(destination);
		return ftp.retrieveFile(source, out);
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public FTPClient getFtp() {
		if (ftp == null) {
			try {
				ftp = new FTPClient();

				ftp.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));

				ftp.connect(server, port);
				int reply = ftp.getReplyCode();
				if (!FTPReply.isPositiveCompletion(reply)) {
					ftp.disconnect();
					throw new IOException("Exception in connecting to FTP Server");
				}

				ftp.login(user, password);
			} catch (IOException e) {
				return null;
			}
		}
		return ftp;
	}

}
