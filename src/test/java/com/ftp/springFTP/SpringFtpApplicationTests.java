package com.ftp.springFTP;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringFtpApplicationTests {

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void givenRemoteFile_whenDownloading_thenItIsOnTheLocalFilesystem() throws IOException {
	    String ftpUrl = String.format(
	      "ftp://equus:root123@127.0.0.1:21/home/equus/ftp/ima.csv");
	 
	    URLConnection urlConnection = new URL(ftpUrl).openConnection();
	    InputStream inputStream = urlConnection.getInputStream();
	    Files.copy(inputStream, new File("downloaded_buz.txt").toPath());
	    inputStream.close();
	 
	    assertThat(new File("downloaded_buz.txt")).exists();
	 
	    new File("downloaded_buz.txt").delete(); // cleanup
	}

}
